#! /bin/bash

# Download and install "protoc" command from https://github.com/protocolbuffers/protobuf/releases/tag/v3.12.2
# Or Install proto3 from source
#  brew install autoconf automake libtool
#  git clone https://github.com/google/protobuf
#  ./autogen.sh ; ./configure ; make ; make install
#
# Update protoc Go bindings via
#  go get -u github.com/golang/protobuf/{proto,protoc-gen-go}
#
# See also
#  https://github.com/grpc/grpc-go/tree/master/examples


# Generate Event  Service
protoc --go_out=plugins=grpc:../../pkg ./v1/*.proto

# Generate Pagination Struct
#  protoc --proto_path=. --go_out=plugins=grpc:../../pkg pagination.proto