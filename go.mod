module masterdata

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-kit/kit v0.10.0
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.3.0
	github.com/golang/protobuf v1.4.2
	github.com/guregu/null v4.0.0+incompatible
	github.com/jinzhu/gorm v1.9.14
	github.com/oklog/oklog v0.3.2
	github.com/opentracing/opentracing-go v1.2.0
	github.com/prometheus/client_golang v1.7.1
	github.com/satori/go.uuid v1.2.0
	github.com/sony/gobreaker v0.4.1
	github.com/stretchr/testify v1.6.1
	github.com/vietta-net/agokit v0.1.25
	golang.org/x/net v0.0.0-20200707034311-ab3426394381
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e
	google.golang.org/grpc v1.30.0
	google.golang.org/protobuf v1.25.0
)
