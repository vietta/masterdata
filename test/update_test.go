package test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/vietta-net/agokit/helper"
	sts "github.com/vietta-net/agokit/status"
	"google.golang.org/grpc/codes"
	"masterdata/internal/app/service"
	"masterdata/internal/pkg/model"
	"masterdata/pkg/pb"
	"testing"
)

var (
    _ =  codes.OK
)

func TestUpdate(t *testing.T) {

	ctx = SetContext()
	cases := []pb.Data{
		{
			Group:       "R",
			Type:        "W",
			Code:        "C",
			Name:        "Cycle Count",
			Description: "Cycle count in the warehouse Proccess",
			Ordering:    1,
			Status:      sts.Active.String(),
		},

	}

	Repo := service.NewRepository(ctx, DB, &Mws)
	for _, tc := range cases {
		t.Run(fmt.Sprintf("Name:%s", tc.Name), func(t *testing.T) {
			err = Repo.Create(&tc)
			if err != nil {
				t.Log("Create: "+ tc.Name)
			}
		})
	}

	m := model.Data{
		Group:       "R",
		Type:        "W",
		Code:        "C",
	}

	DB.Where("code = ?", m.Code).First(&m)

	if m.ID ==""{
		return
	}
	t.Log(m.ID)

	data := &pb.Data{
		Id: m.ID,
		Group:       "R",
		Type:        "W",
		Code:        "C",
		Name:        "Cycle Counting",
		Description: "Cycle count in the warehouse management",
		Ordering:    0,
		Status:      sts.Active.String(),
	}

	req := &pb.UpdateRequest{
		Api: "v1",
		Data: data,
	}
	res, err := svc.Update(ctx, req)

	assert.Nil(t, err)

	assert.Equal(t, res.Status.Message, "Updated")
    assert.Equal(t, res.Status.Code, uint32(codes.OK))

	//Test Empty Name
	d := pb.Data{}
	helper.Convert(&d, &data)
	d.Name = ""
	err = Repo.Update(&d)
	assert.Error(t, err)
	t.Log(err)

	//Test Empty Status
	d = pb.Data{}
	helper.Convert(&d, &data)
	d.Status =""
	err = Repo.Update(&d)
	assert.Error(t, err)
	t.Log(err)

	//Test Invalid Status
	d = pb.Data{}
	helper.Convert(&d, &data)
	d.Status ="01"
	err = Repo.Update(&d)
	assert.Error(t, err)
	t.Log(err)

	//Test delete data
	dreq := &pb.DeleteRequest{
		Api: "v1",
		Id: data.Id,
	}
	dres, err := svc.Delete(ctx, dreq)

	assert.Nil(t, err)
	assert.Equal(t, dres.Status.Message, "Deleted")
	assert.Equal(t, dres.Status.Code, uint32(codes.OK))
}