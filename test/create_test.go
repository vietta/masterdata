package test

import (
	"github.com/stretchr/testify/assert"
	"github.com/vietta-net/agokit/helper"
	sts "github.com/vietta-net/agokit/status"
	"google.golang.org/grpc/codes"
	"masterdata/internal/app/service"
	"masterdata/internal/pkg/model"
	"masterdata/pkg/pb"
	"testing"
)

var (
    _ =  codes.OK
)

func TestCreate(t *testing.T) {
	ctx = SetContext()
	data := &pb.Data{
		Group:       "R",
		Type:        "T",
		Code:        "RT",
		Name:        "Request Type",
		Description: "",
		Ordering:    0,
		Status:      sts.Active.String(),
	}

	m := model.Data{}
	DB.Where("code = ?", "RT").First(&m)

	if  m.ID != "" {
		return
	}

	req := &pb.CreateRequest{
		Api: "v1",
		Data: data,
	}



	res, err := svc.Create(ctx, req)

	assert.Nil(t, err)

	assert.Equal(t, res.Status.Message, "Created")
    assert.Equal(t, res.Status.Code, uint32(codes.OK))

	Repo := service.NewRepository(ctx, DB, &Mws)

	//Test Invalid Status
	d := pb.Data{}
	helper.Convert(&d, &data)
	d.Status = `01`
	err = Repo.Create(&d)
	assert.Error(t, err)
	t.Log(err)

	//Test Empty Group
	d = pb.Data{}
	helper.Convert(&d, &data)
	d.Group = ""
	err = Repo.Create(&d)
	assert.Error(t, err)

	t.Log(err)

	//Test Empty Type
	d = pb.Data{}
	helper.Convert(&d, &data)
	d.Type =""
	err = Repo.Create(&d)
	assert.Error(t, err)
	t.Log(err)

	//Test Empty Name
	d = pb.Data{}
	helper.Convert(&d, &data)
	d.Name =""
	err = Repo.Create(&d)
	assert.Error(t, err)
	t.Log(err)

	//Test Empty Code
	d = pb.Data{}
	helper.Convert(&d, &data)
	d.Code =""
	err = Repo.Create(&d)
	assert.Error(t, err)
	t.Log(err)

	//Test Empty Status
	d = pb.Data{}
	helper.Convert(&d, &data)
	d.Status =""
	err = Repo.Create(&d)
	assert.Error(t, err)
	t.Log(err)


	//Test Read Data
	rReq := &pb.ReadRequest{
		Api: "v1",
		Id: data.Id,
	}

	_, err = svc.Read(ctx, rReq)

	assert.Nil(t, err)

}


