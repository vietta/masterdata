package test

import (
	"fmt"
	stdjwt "github.com/dgrijalva/jwt-go"
	"github.com/go-kit/kit/auth/jwt"
	"github.com/jinzhu/gorm"
	"github.com/vietta-net/agokit/config"
	"github.com/vietta-net/agokit/gokit"
	"google.golang.org/grpc/metadata"
	"log"
	"masterdata/internal/pkg/model"
	iservice "masterdata/pkg/service"
	"masterdata/internal/app/service"
	"os"
	"path/filepath"

	"testing"
	"github.com/stretchr/testify/assert"
	"context"
)

var (
	cfg *config.BasicConfig
	err error
	DB *gorm.DB
	Mws config.Middlewares

	svc iservice.Service
	ctx = context.Background()

	ServerTimezone = "Asia/Ho_Chi_Minh"

)

var _ = func() bool {
	testing.Init()
	return true
}()
func init(){
	if DB != nil {
		return
	}
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	parent := filepath.Dir(path)

	var con config.Config
	con, err = config.New(parent)
	cfg = con.(*config.BasicConfig)
	Mws = cfg.LoadMiddlewares()
	DB, err = cfg.LoadDB()
	svc    = service.New(cfg)
	_   = SetContext()


	DB.AutoMigrate(&DataModel{})
	fmt.Println(DB.Error)
}

type DataModel struct {
	gorm.Model
	model.Data
}

func SetContext()(ctx context.Context){
	ctx = context.Background()
	tokenString := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJwbnRuNzkiLC" +
		"Jpc3MiOiJKb2huIERvZSIsImF1ZCI6IldlYiJ9.v0ftayqSMDefdanGepNm8aYWWI3JTJ5zyTsdrhguQKs"

	//https://tools.ietf.org/html/rfc4647
	md := metadata.Pairs(
		"content-language", "vi",
		"timezone", "Asia/Ho_Chi_Minh",
		"authorization" , "Bearer " + tokenString)
	ctx = metadata.NewOutgoingContext(ctx, md)

	kf := func(token *stdjwt.Token) (interface{}, error) { return []byte(cfg.App.Secret), nil }
	token, _ := stdjwt.ParseWithClaims(tokenString, jwt.StandardClaimsFactory(),kf)
	ctx 	= context.WithValue(ctx, jwt.JWTClaimsContextKey, token.Claims)

	// capital "Key" is illegal in HTTP/2.
	timezone, ok := md[gokit.TimezoneKey]
	tz := ServerTimezone
	if ok {
		tz = timezone[0]
	}
	ctx = context.WithValue(ctx, gokit.TimezoneKey, tz)

	return ctx
}

func TestInit(t *testing.T) {
	assert.NotNil(t, DB)
	assert.NotNil(t, cfg.App.Name)
}