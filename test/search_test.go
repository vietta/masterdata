package test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	sts "github.com/vietta-net/agokit/status"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/timestamppb"
	"masterdata/internal/app/service"
	"masterdata/pkg/pb"
	"testing"
	"time"
)

var (
	_ =  codes.OK
)

func TestSearch(t *testing.T) {
	ctx = SetContext()
	cases := []pb.Data{
		{
			Group:       "R01",
			Type:        "I",
			Code:        "R",
			Name:        "Goods Receipt",
			Description: "Receipt in the inbound Proccess",
			Ordering:    1,
			Status:      sts.Active.String(),
		},
		{
			Group:       "R01",
			Type:        "W",
			Code:        "A",
			Name:        "Adjustment",
			Description: "Adjustment at Warehouse Inventory Management",
			Ordering:    2,
			Status:      sts.Active.String(),
		},
		{
			Group:       "R01",
			Type:        "O",
			Code:        "B",
			Name:        "Bulk Order",
			Description: "Bulk Order in the outbound Proccess",
			Ordering:    3,
			Status:      sts.Active.String(),
		},
		{
			Group:       "R01",
			Type:        "O",
			Code:        "P",
			Name:        "Parcel Order",
			Description: "Parcel Order in the outbound Proccess",
			Ordering:    4,
			Status:      sts.Active.String(),
		},
	}

	Repo := service.NewRepository(ctx, DB, &Mws)
	for _, tc := range cases {
		t.Run(fmt.Sprintf("Create/%s", tc.Name), func(t *testing.T) {
			err = Repo.Create(&tc)
			t.Log("Create: "+ tc.Name)
		})
	}

	//Search Data
	query := &pb.Query{
		Limit: 2,
		Page: 1,
		Keyword: "bulk",
	}
	filter := &pb.Filter{
		Group:       []string{"R01"},
		Type:        []string{"O"},
		Code: 		 "B",
	}

	req := &pb.SearchRequest{
		Api: "v1",
		Query: query,
		Filter: filter,
	}

	searchCases := GetSearchCases()
	for _, tc := range searchCases {
		t.Run(fmt.Sprintf("Search/%s", tc.Name), func(t *testing.T) {
			t.Log("Case: "+ tc.Name)

			req.Filter = &tc.Filter
			req.Query = &tc.Query
			res, err := svc.Search(ctx, req)
			if tc.Expectation.Error {
				assert.Error(t, err)
			}else{
				assert.Nil(t, err)
				assert.Equal(t, tc.Expectation.StatusCode, res.Status.Code)
				assert.Equal(t, tc.Expectation.Records, len(res.Data) )
				assert.Equal(t, tc.Expectation.Pagination.Count, res.Pagination.Count)
				assert.Equal(t, tc.Expectation.Pagination.Limit, res.Pagination.Limit)
				assert.Equal(t, tc.Expectation.Pagination.Pages, res.Pagination.Pages)
				assert.Equal(t, tc.Expectation.Pagination.Page, res.Pagination.Page)
			}

		})
	}

}

type Expectation struct {
	Pagination pb.Pagination
	Records int
	StatusCode uint32
	Error bool
}

type SearchCase struct {
	Name string
	Query pb.Query
	Filter pb.Filter
	Expectation Expectation
}

func GetSearchCases() []SearchCase  {
	format := "2006-01-02"
	from, _ := time.Parse(format, "2010-07-13")
	to, _ := time.Parse(format, "2010-07-13")

	Emptyfrom, _ := time.Parse(format, "")
	Emptyto, _ := time.Parse(format, "")

	from2010, _ := time.Parse(format, "2010-07-13")

	data := []SearchCase{
		{
			Name: "Date To is less than Date From",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Dates: []*pb.DateRange{
					{
						From: timestamppb.Now(),
						To: timestamppb.New(from2010),
					},
				},
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{},
				Records: 0,
				StatusCode: uint32(codes.InvalidArgument),
				Error: true,
			},
		},
		{
			Name: "Not Found Date Range",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Dates: []*pb.DateRange{
					{
						From: timestamppb.New(from),
						To: timestamppb.New(to),
					},
				},
			},
			Filter: pb.Filter{

			},
			Expectation: Expectation{
				Pagination: pb.Pagination{},
				Records: 0,
				StatusCode: uint32(codes.NotFound),
				Error: true,
			},
		},

		{
			Name: "Empty Date To",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Dates: []*pb.DateRange{
					{
						From: timestamppb.New(from2010),
						To: timestamppb.New(Emptyto),
					},
				},
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 4,
				},
				Records: 4,
				StatusCode: uint32(codes.OK),
			},
		},
		{
			Name: "Empty DateRange",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Dates: []*pb.DateRange{
					{
						From: timestamppb.New(Emptyfrom),
						To: timestamppb.New(Emptyto),
					},
				},
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 4,
				},
				Records: 4,
				StatusCode: uint32(codes.OK),
			},
		},

		{
			Name: "Filter Multiple Types",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
				Type: []string{"I", "W"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 2,
				},
				Records: 2,
			},
		},

		{
			Name: "Empty DateRange",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Dates: []*pb.DateRange{
					{
						From: timestamppb.New(Emptyfrom),
						To: timestamppb.New(Emptyto),
					},
				},
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 4,
				},
				Records: 4,
				StatusCode: uint32(codes.OK),
			},
			},
		{
			Name: "Empty Date From",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Dates: []*pb.DateRange{
					{
						From: timestamppb.New(Emptyfrom),
						To: timestamppb.Now(),
					},
				},
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 4,
				},
				Records: 4,
				StatusCode: uint32(codes.OK),
			},
		},


		{
			Name: "Not Found",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
			},
			Filter: pb.Filter{
				Group: []string{"R0112"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{},
				Records: 0,
				StatusCode: uint32(codes.NotFound),
				Error: true,
			},
		},
		{
			Name: "Filter Group",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 4,
				},
				Records: 4,
			},
		},

		{
			Name: "Total Pages",
			Query: pb.Query{
				Limit: 2,
				Page: 1,
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 2,
					Page: 1,
					Pages: 2,
					Count: 4,
				},
				Records: 2,
			},
		},
		{
			Name: "Search Proccess",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Keyword: "Proccess",
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 3,
				},
				Records: 3,
			},
		},
		{
			Name: "Search bulk",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Keyword: "bulk",
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 1,
				},
				Records: 1,
			},
		},
		{
			Name: "Search Proccess and Filter Type",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Keyword: "Proccess",
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
				Type: []string{"O"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 2,
				},
				Records: 2,
			},
		},
		{
			Name: "Search Proccess and Filter Type, Code",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Keyword: "Proccess",
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
				Type: []string{"O"},
				Code: "B",
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 1,
				},
				Records: 1,
			},
		},
		{
			Name: "Search Management in Description",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Keyword: "Management",
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 1,
				},
				Records: 1,
			},
		},
		{
			Name: "Search Goods in Name",
			Query: pb.Query{
				Limit: 40,
				Page: 1,
				Keyword: "Goods",
			},
			Filter: pb.Filter{
				Group: []string{"R01"},
			},
			Expectation: Expectation{
				Pagination: pb.Pagination{
					Limit: 40,
					Page: 1,
					Pages: 1,
					Count: 1,
				},
				Records: 1,
			},
		},
	}
	return data
}