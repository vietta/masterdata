
-- +migrate Up
CREATE TABLE `data` (
  mode tinyint(1) UNSIGNED NOT NULL COMMENT '1 - Real data, 0 - Test',
  id char(36) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL COMMENT 'md5(mode, group, type, code)',
  parent char(36) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL,
  `group` varchar(3) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  type varchar(3) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  code varchar(5) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  name varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  description varchar(110) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  ordering tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `deletable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  status varchar(2) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  created_by varchar(10) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL COMMENT 'Username',
  created_at datetime NOT NULL,
  updated_at datetime NOT NULL,
  deleted_at datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


ALTER TABLE `data`
  ADD PRIMARY KEY (id),
  ADD UNIQUE KEY unique_key (`group`,`type`,`code`),
  ADD KEY parent (parent);

-- +migrate Down

DROP TABLE `data`;