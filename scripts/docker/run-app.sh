#! /bin/bash

docker stop masterdata
docker rm masterdata

#docker run -p 8082:8082 -p 64000:64000 --name masterdata -d pntn79/masterdata:latest

#Wait for Mysql to be ready
for i in `seq 1 10`;
do
  nc -z 127.0.0.1 3307 ; break
  echo -n .
  sleep 1
done


docker run --network container:mysql  --name masterdata -d pntn79/masterdata:latest

RESULT=`docker exec -it masterdata nc -vz 127.0.0.1 8080`

if [[ "$RESULT" == *"127.0.0.1"* ]]; then
    echo "Service is running" && exit 0
else
     echo "Service is not running" && exit 1
fi