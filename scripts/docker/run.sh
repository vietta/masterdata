#! /bin/bash

CONTAINER_NAME="mysql"
MYSQL_ROOT_PASSWORD="rootpw"
MYSQL_DATABASE="masterdata-dev"
MYSQL_USER="dev"
MYSQL_PASSWORD="passw0rd"

#Stop and delete mysql to make sure an empty database for running unit test

docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME

docker stop zipkin
docker rm zipkin

docker stop appdash
docker rm appdash

docker run \
--name $CONTAINER_NAME \
-e MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD \
-e MYSQL_DATABASE=$MYSQL_DATABASE \
-e MYSQL_USER=$MYSQL_USER \
-e MYSQL_PASSWORD=$MYSQL_PASSWORD \
-p 3307:3306 \
-p 9411:9411 \
-p 7701:7701 \
-p 7700:7700 \
-p 8082:8082 \
-p 8080:8080 \
-p 64000:64000 \
-d mysql:latest \
--default-authentication-plugin=mysql_native_password \
--character-set-server=utf8mb4 \
--collation-server=utf8mb4_unicode_ci

docker run --network container:mysql  --name zipkin -d openzipkin/zipkin

docker run --network container:mysql  --name appdash -d mcesar/appdash:latest








