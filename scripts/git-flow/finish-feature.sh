#! /bin/bash

BRANCH=`git symbolic-ref --short -q HEAD`

if [ "$BRANCH" != "master" ]; then
    cd ..
    cd ..
    git push origin "$BRANCH"
    git checkout develop
    git merge "$BRANCH"
    git flow feature finish "$BRANCH"

else
    echo "Current branch $BRANCH is not a feature branch stead feature"
fi