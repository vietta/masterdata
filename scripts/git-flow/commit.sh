#! /bin/bash

BRANCH=`git symbolic-ref --short -q HEAD`

if [ "$1" != "" ]; then
  if [ "$BRANCH" != "master" ]; then
    cd ..
    cd ..
    git add .
    git commit -m "$1"
  else
    echo "Current branch $BRANCH stead of a feature branch"
  fi
else
    echo "Please enter a message"
fi