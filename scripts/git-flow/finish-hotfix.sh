#! /bin/bash

BRANCH=`git symbolic-ref --short -q HEAD`
if [ "$BRANCH" != "master" ]; then
    cd ..
    cd ..
    git checkout master
    git merge "$BRANCH"
    git checkout develop
    git merge "$BRANCH"
    git flow hotfix finish "$BRANCH"
else
    echo "Current branch $BRANCH is not a hotfix branch stead hotfix/*"
fi