#! /bin/bash

if [ "$1" != "" ]; then
  cd ..
  cd ..
  git checkout master
  git pull
  git checkout develop
  git merge master
  git pull origin develop
  git checkout -b "$1"
  git flow feature start "$1"
else
    echo "Please enter a branch name"
fi