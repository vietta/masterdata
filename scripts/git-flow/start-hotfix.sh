#! /bin/bash

#Use for bug on master branch

if [ "$1" != "" ]; then
  cd ..
  cd ..
  git checkout master
  git pull origin master
  git checkout -b "$1"
  git flow hotfix start "$1"
else
    echo "Please enter a branch name"
fi