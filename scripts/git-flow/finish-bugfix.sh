#! /bin/bash

#Use for bug on feature branch

BRANCH=`git symbolic-ref --short -q HEAD`

if [[ "$BRANCH" == *"bugfix/feature/"*  ]]; then

    BUG_BRANCH="$(cut -d'/' -f 4 <<< $BRANCH)"
    if [ "$BUG_BRANCH" != "" ]; then
      FEATURE_NAME="$(cut -d'/' -f 3 <<< $BRANCH)"
      FEATURE_BRANCH="feature/${FEATURE_NAME}"
      git checkout $FEATURE_BRANCH
      git merge "$BRANCH"

      git push origin "$FEATURE_BRANCH"

      git branch -d $BRANCH

    else
      echo "Current branch $BRANCH is not a bug feature branch like bug/feature/<feature name>/<bug name>"
    fi

else
    echo "Current branch $BRANCH is not a bug feature branch like bug/feature/<feature name>/<bug name>"
fi