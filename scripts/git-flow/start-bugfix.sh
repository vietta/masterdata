#! /bin/bash

#Use for bug on feature branch

BRANCH=`git symbolic-ref --short -q HEAD`
if [[ "$BRANCH" == *"feature/"* ]]; then

    if [ "$1" != "" ]; then
      cd ..
      cd ..
      #Pull a latest source code
      git checkout $BRANCH
      git pull origin $BRANCH
      #Create a new branch for fixing bug
      git checkout -b "bugfix/$BRANCH/$1"
    else
        echo "Please enter a bug branch name"
    fi

else
    echo "Current branch $BRANCH is not a feature branch like feature/<name>"
fi