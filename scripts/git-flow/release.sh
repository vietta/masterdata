#! /bin/bash

GITURL=`git config --get remote.origin.url`

if [ "$GITURL" != "" ]; then
  #Get the highest tag number
  #VERSION=`git describe --abbrev=0 --tags`
  VERSION=`git ls-remote --tags --refs --sort="version:refname" $GITURL | awk -F/ 'END{print$NF}'`
  VERSION=${VERSION:-'0.1.0'}

  #Get number parts
  MAJOR="${VERSION%%.*}"; VERSION="${VERSION#*.}"
  MINOR="${VERSION%%.*}"; VERSION="${VERSION#*.}"
  PATCH="${VERSION%%.*}"; VERSION="${VERSION#*.}"

  #Get current hash and see if it already has a tag
  GIT_COMMIT=`git rev-parse HEAD`
  NEEDS_TAG=`git describe --contains $GIT_COMMIT`

  #Only tag if no tag already (would be better if the git describe command above could have a silent option)
  if [ -z "$NEEDS_TAG" ]; then
      #Go back to root
      cd ..
      cd ..

      #Increase version
      PATCH=$((PATCH+1))
      #Create new Release
      NEW_RELEASE="$MAJOR.$MINOR.$PATCH"

      git checkout develop
      git checkout -b "release/$NEW_RELEASE"
      git push origin "release/$NEW_RELEASE"

      git checkout develop
      git merge "release/$NEW_RELEASE"
      git push origin develop

      git flow release start "$NEW_RELEASE"
      git checkout master
      git merge "release/$NEW_RELEASE"
      git push origin master
  else
      VERSION="$MAJOR.$MINOR.$PATCH"
      echo "Already a release (version $VERSION) on this commit"
  fi
else
    echo "Error: (Update your git url in GITURL)"
fi

