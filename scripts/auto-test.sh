#!/bin/sh

cd ..
cd db
sql-migrate up

cd ..
go test ./... -v -cover


cd db
sql-migrate down
