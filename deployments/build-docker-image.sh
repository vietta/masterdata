#! /bin/bash
cd ..

GITURL=`git config --get remote.origin.url`
VERSION=`git ls-remote --tags --refs --sort="version:refname" $GITURL | awk -F/ 'END{print$NF}'`
VERSION=${VERSION:-'0.1.0'}

#Get number parts
MAJOR="${VERSION%%.*}"; VERSION="${VERSION#*.}"
MINOR="${VERSION%%.*}"; VERSION="${VERSION#*.}"
PATCH="${VERSION%%.*}"; VERSION="${VERSION#*.}"
PATCH=$((PATCH+1))
NEW_VERSION="$MAJOR.$MINOR.$PATCH"

docker build -t pntn79/masterdata:$NEW_VERSION .

docker login -u "$DOCKER_USER" -p "$DOCKER_PASS"

docker push pntn79/masterdata:$NEW_VERSION