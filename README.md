Coding guide:

1. Run migration: Create new table, dump data,
    masterdata$ cd ./db
    # Run these commands
    masterdata$ sql-migrate new create_masterdata_table -env="local"

    # sql-migrate down -env="local"

    # sql-migrate up -env="local"

2. Coding: Business logic and rules
    ./internal/service/<version>/service.go

3. Coding: Query and execute transactions
    ./internal/service/<version>/respository.go

4. Create models (adapt to db table)
    ./internal/pkg/model
        <table name  1>.go
        <table name  2>.go

5. Add more middlewares or external services
    ./internal/service/
    ./internal/service/<version>/

6. Run server
    masterdata$ go run cmd/server/masterdata-grpc.go -config-path=./configs
    masterdata$ go run cmd/client/masterdata-client.go