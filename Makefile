GITURL=$(shell git config --get remote.origin.url)

VERSION=$(shell git ls-remote --tags --refs --sort="version:refname" $GITURL | awk -F/ 'END{print$NF}')
VERSION=$(shell ${VERSION:-'0.1.0'})
MAJOR=$(shell "${VERSION%%.*}"; VERSION="${VERSION#*.}")
MINOR=$(shell "${VERSION%%.*}"; VERSION="${VERSION#*.}")
PATCH=$(shell "${VERSION%%.*}"; VERSION="${VERSION#*.}")
PATCH=$(shell $((PATCH+1)) )

TAG ==$(shell "$MAJOR.$MINOR.$PATCH")

VENDORS = $(shell go get -v -t -d ./...  | grep -v /vendor/)
GOFILES = $(shell find . -name '*.go' -not -path './vendor/*')
GOPACKAGES = $(shell go list ./...  | grep -v /vendor/)

default: build

workdir:
	mkdir -p workdir

build: workdir/masterdata

build-native: $(GOFILES)
	go build -o workdir/native-masterdata ./cmd/server

workdir/masterdata: $(GOFILES)
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -ldflags="-w -s" -o workdir/masterdata ./cmd/server

test:
	test-all

test-all:
	go test -v $(GOPACKAGES)