package model

import (
	"fmt"

	"github.com/vietta-net/agokit/errors"
	sts "github.com/vietta-net/agokit/status"
	"google.golang.org/grpc/codes"

	//TODO: Update validate
	//_  "github.com/go-playground/universal-translator"
	//_ "github.com/go-playground/validator/v10"
	//_ "github.com/go-playground/locales/en"
	//_  "github.com/go-playground/validator/v10/translations/en"
)

// Validate invoked before performing action, return an error if field is not populated.
func (d *Data) Validate(action Action) error {
	var errs = make(map[string]string)
	//Use Validator tool to validate

	if d.Group =="" {
		errs["Group"] = "Group should not empty!"
	}
	if d.Type =="" {
		errs["Type"] = "Type should not empty!"
	}
	if d.Code =="" {
		errs["Code"] = "Code should not empty!"
	}
	if d.Name =="" {
		errs["Name"] = "Name should not empty!"
	}
	if d.Status =="" {
		errs["Status"] = "Status should not empty!"
	}

	//Manually Validate
	if action == Create {
		if sts.Active.NotEqual(d.Status){
			errs["Status"] = fmt.Sprintf("Status Code should be `%s`, but Status Code %s", sts.Active,d.Status)
		}
	}

	if action == Update {
		if  d.ID =="" {
			errs["ID"] = "ID should not empty!"
		}
		if ! (sts.Active.Equal(d.Status) || sts.Inactive.Equal(d.Status)){
			errs["Status"] = fmt.Sprintf("Status Code should be `%s` or `%s`, but Status Code %s", sts.Active, sts.Inactive, d.Status)
		}
	}

	if len(errs) > 0 {
		return errors.E(
			codes.InvalidArgument,
			"InvalidArgument Error, please check",
			errs)
	}

	return nil
}
