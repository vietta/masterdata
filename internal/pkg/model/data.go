package model

import (
	"database/sql"
	"time"

	"github.com/guregu/null"
	"github.com/satori/go.uuid"
)

var (
	_ = time.Second
	_ = sql.LevelDefault
	_ = null.Bool{}
	_ = uuid.UUID{}
)

/*
DB Table Details
-------------------------------------


CREATE TABLE `data` (
  `mode` tinyint unsigned NOT NULL COMMENT '1 - Real data, 0 - Test',
  `id` char(36) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL COMMENT 'md5(mode, group, type, code)',
  `parent` char(36) CHARACTER SET ascii COLLATE ascii_general_ci DEFAULT NULL,
  `group` varchar(3) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  `type` varchar(3) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  `code` varchar(5) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(110) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `ordering` int unsigned NOT NULL DEFAULT '0',
  `deletable` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(2) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL,
  `created_by` varchar(10) CHARACTER SET ascii COLLATE ascii_general_ci NOT NULL COMMENT 'Username',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_2` (`group`,`type`,`code`),
  KEY `mode` (`mode`),
  KEY `status` (`mode`,`status`) USING BTREE,
  KEY `group` (`mode`,`group`) USING BTREE,
  KEY `type` (`mode`,`group`,`type`) USING BTREE,
  KEY `parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci

JSON Sample
-------------------------------------
{    "parent": "JIwSuwLwySUUCvuFCeUvVkHCX",    "created_at": "2216-12-24T07:06:19.454823741+07:00",    "name": "stZynMTGnOHXagtAwdliAxWiR",    "deletable": 71,    "id": "YlwaERNyuAeCTehXQrRABbNnV",    "type": "GvLtkSupMQJYMjZRXKuPhaXxX",    "code": "KLGAbDWLUnUEmwuqIBdiCNmJT",    "ordering": 18,    "created_by": "LTvZhEvqAMXcXCFturuOleYyC",    "updated_at": "2129-06-21T12:53:32.512592044+07:00",    "deleted_at": "2308-09-13T04:28:20.342917081+07:00",    "mode": 8,    "group": "svuigTwpnTArwstnROFpNjkZT",    "description": "PLNPtZQHfgPAqNqAhEahLdbDs",    "status": "cNnBqXuGhgZIwbmGIiDwBTwtu"}


Comments
-------------------------------------
[ 0] column is set for unsigned
[ 8] column is set for unsigned



*/

// Data struct is a row record of the data table in the masterdata-dev database
type Data struct {
	//[ 0] mode                                           utinyint             null: false  primary: false  isArray: false  auto: false  col: utinyint        len: -1      default: []
	Mode uint32 `gorm:"column:mode;type:tinyint;size:1" json:"mode"`
	//[ 1] id                                             char(36)             null: false  primary: true   isArray: false  auto: false  col: char            len: 36      default: []
	ID string `gorm:"primary_key;column:id;type:char;size:36;" json:"id"`
	//[ 2] parent                                         char(36)             null: true   primary: false  isArray: false  auto: false  col: char            len: 36      default: []
	Parent null.String `gorm:"column:parent;type:char;size:36;" json:"parent"`
	//[ 3] group                                          varchar(3)           null: false  primary: false  isArray: false  auto: false  col: varchar         len: 3       default: []
	Group string `gorm:"column:group;type:varchar(3);size:3;" json:"group"`
	//[ 4] type                                           varchar(3)           null: false  primary: false  isArray: false  auto: false  col: varchar         len: 3       default: []
	Type string `gorm:"column:type;type:varchar(3);size:3;" json:"type"`
	//[ 5] code                                           varchar(5)           null: false  primary: false  isArray: false  auto: false  col: varchar         len: 5       default: []
	Code string `gorm:"column:code;type:varchar(3);size:5;" json:"code"`
	//[ 6] name                                           varchar(30)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 30      default: []
	Name string `gorm:"column:name;type:varchar(30);size:30;" json:"name"`
	//[ 7] description                                    varchar(110)         null: true   primary: false  isArray: false  auto: false  col: varchar         len: 110     default: []
	Description null.String `gorm:"column:description;type:varchar(110);size:110;" json:"description"`
	//[ 8] ordering                                       uint                 null: false  primary: false  isArray: false  auto: false  col: uint            len: -1      default: [0]
	Ordering uint32 `gorm:"column:ordering;type:int;default:0;" json:"ordering"`
	//[ 9] status                                         varchar(2)           null: false  primary: false  isArray: false  auto: false  col: varchar         len: 2       default: []
	Status string `gorm:"column:status;type:varchar(2);size:2;" json:"status"`
	//[10] created_by                                     varchar(10)          null: false  primary: false  isArray: false  auto: false  col: varchar         len: 10      default: []
	CreatedBy string `gorm:"column:created_by;type:varchar(10);size:10;" json:"created_by"`
	//[11] created_at                                     datetime             null: false  primary: false  isArray: false  auto: false  col: datetime        len: -1      default: []
	CreatedAt time.Time `gorm:"column:created_at;type:datetime;" json:"created_at"`
	//[12] updated_at                                     datetime             null: false  primary: false  isArray: false  auto: false  col: datetime        len: -1      default: []
	UpdatedAt time.Time `gorm:"column:updated_at;type:datetime;" json:"updated_at"`
	//[13] deleted_at                                     datetime             null: true   primary: false  isArray: false  auto: false  col: datetime        len: -1      default: []
	DeletedAt null.Time `gorm:"column:deleted_at;type:datetime;" json:"deleted_at"`
	//[14] Deletable
	Deletable uint32 `gorm:"column:deletable;type:tinyint;size:1" json:"deletable"`
}

// TableName sets the insert table name for this struct type
func (d *Data) TableName() string {
	return "data"
}

// BeforeSave invoked before saving, return an error if field is not populated.
func (d *Data) BeforeSave() error {
	d.ID = uuid.NewV1().String()
	return nil
}

// Prepare invoked before saving, can be used to populate fields etc.
func (d *Data) Prepare() {
}


