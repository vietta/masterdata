package model_test

import (
	"github.com/vietta-net/agokit/errors"
	"testing"
	"github.com/stretchr/testify/assert"

	sts "github.com/vietta-net/agokit/status"
	"masterdata/internal/pkg/model"
)

func TestValidate(t *testing.T) {

	m := &model.Data{
		Type:        "T",
		Code:        "RT",
		Name:        "Request Type",
		Ordering:    0,
		Status:      sts.Active.String(),
	}
	err := m.Validate(model.Create)
	t.Log(err.(*errors.Error).Error())
	assert.Error(t, err)
}
