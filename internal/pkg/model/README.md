#Model here

import (
	"time"
)

type Model struct {

}

func (m *Model) BeforeSave() (err error) {
	m.CreatedAt = uint64(time.Now().Unix())
	m.UpdatedAt = uint64(time.Now().Unix())
	uuid, _ := uuid.NewV1()
	id := uuid.String()
	m.Id = id
	return nil
}