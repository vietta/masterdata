package service

var (
	UnsupportedAPI = "Unsupported API version: service implements API version '{{.ServerApi}}', but asked for '{{.ClientApi}}'"
)
