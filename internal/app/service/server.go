package service

import (
	"fmt"
	kitgrpc "github.com/go-kit/kit/transport/grpc"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/oklog/oklog/pkg/group"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	config "github.com/vietta-net/agokit/config"
	context "golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	endpoints "masterdata/pkg/endpoints"
	pb "masterdata/pkg/pb"
	grpctransports "masterdata/pkg/transports/grpc"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

var (
	acceptLanguage = "en, en-US, vi"

)

// RunServer runs gRPC server and HTTP gateway
func Run(basePath string) error {
	ctx := context.Background()
	cfgs, err := config.New(basePath)
	cfg := cfgs.(*config.BasicConfig)
	mws := cfg.LoadMiddlewares()

	db, err := cfg.LoadDB()
	if err != nil {
		mws.Logger.Log("Database Rrror: ", err)
		os.Exit(int(codes.Unavailable))
	}

    defer db.Close()

    if mws.Reporter != nil {
        defer mws.Reporter.Close()
    }

    if mws.LightstepTracer != nil {
        defer mws.LightstepTracer.Close(ctx)
    }

	http.DefaultServeMux.Handle(cfg.Com.Debug.Path, promhttp.Handler())


	// Build the layers of the service "onion" from the inside out. First, the
	// business logic service; then, the set of endpoints that wrap the service;
	// and finally, a series of concrete transport adapters. The adapters, like
	// the HTTP handler or the gRPC server, are the bridge between Go kit and
	// the interfaces that the transports expect. Note that we're not binding
	// them to ports or anything yet; we'll do that next.
	var (
		service    = New(cfg)
		endpoints  = endpoints.MakeEndpoints(service, cfg)
		grpcServer = grpctransports.MakeGRPCServer(endpoints,cfg)
	)

	// Now we're to the part of the func main where we want to start actually
	// running things, like servers bound to listeners to receive connections.
	//
	// The method is the same for each component: add a new actor to the group
	// struct, which is a combination of 2 anonymous functions: the first
	// function actually runs the component, and the second function should
	// interrupt the first function and cause it to return. It's in these
	// functions that we actually bind the Go kit server/handler structs to the
	// concrete transports and run them.
	//
	// Putting each component into its own block is mostly for aesthetics: it
	// clearly demarcates the scope in which each listener/socket may be used.
	var g group.Group
	{
		// The debug listener mounts the http.DefaultServeMux, and serves up
		// stuff like the Prometheus metrics route, the Go debug and profiling
		// routes, and so on.
		debugAddr := fmt.Sprintf("127.0.0.1:%d", cfg.Com.Debug.Port)
		debugListener, err := net.Listen("tcp", debugAddr)
		if err != nil {
			cfg.Mws.Logger.Log("transport", "debug/HTTP", "during", "Listen", "err", err)
			os.Exit(1)
		}
		g.Add(func() error {
			cfg.Mws.Logger.Log("transport", "debug/HTTP", "addr", debugAddr)
			return http.Serve(debugListener, http.DefaultServeMux)
		}, func(error) {
			debugListener.Close()
		})
	}
	{
		healthAddr := fmt.Sprintf("127.0.0.1:%d", cfg.Com.Rest.Port)
		healthListener, err := net.Listen("tcp", healthAddr)
		if err != nil {
			cfg.Mws.Logger.Log("transport", "health/HTTP", "during", "Listen", "err", err)
			os.Exit(1)
		}
		g.Add(func() error {
			cfg.Mws.Logger.Log("transport", "health/HTTP", "URL", "http://" + healthAddr + "/health")
			mux := http.NewServeMux()
			mux.HandleFunc("/health", func(writer http.ResponseWriter, request *http.Request) {
				writer.WriteHeader(http.StatusOK)
			})
			return http.Serve(healthListener, mux)
		}, func(error) {
			healthListener.Close()
		})
	}

	{
		// The gRPC listener mounts the Go kit gRPC server we created.
		grpcAddress := fmt.Sprintf(":%d", cfg.Com.Grpc.Port)
		grpcListener, err := net.Listen("tcp", grpcAddress)
		if err != nil {
			cfg.Mws.Logger.Log("transport", "gRPC", "during", "Listen", "err", err)
			os.Exit(1)
		}
		g.Add(func() error {
			cfg.Mws.Logger.Log("transport", "gRPC", "addr", grpcAddress)
			// we add the Go Kit gRPC Interceptor to our gRPC service as it is used by
			// the here demonstrated zipkin tracing middleware.
			baseServer := grpc.NewServer(grpc.UnaryInterceptor(kitgrpc.Interceptor))
			pb.RegisterMasterdataServiceServer(baseServer, grpcServer)
			return baseServer.Serve(grpcListener)
		}, func(error) {
			grpcListener.Close()
		})
	}

	if cfg.Arg.Mode ==1 {
    		go func() {
    			time.Sleep(3 * time.Second)
    			os.Exit(int(codes.OK))
    		}()
    }

	{
		// This function just sits and waits for ctrl-C.
		cancelInterrupt := make(chan struct{})
			//TODO: Shutdown tracefull
			g.Add(func() error {
				c := make(chan os.Signal, 1)
				signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
				select {
				case sig := <-c:
				return fmt.Errorf("received signal %s", sig)
				case <-cancelInterrupt:
				return nil
			}
		}, func(error) {
			close(cancelInterrupt)
		})
	}
	cfg.Mws.Logger.Log("exit", g.Run())




	return nil
}
