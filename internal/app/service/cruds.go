package service


import (
	"encoding/json"
	"fmt"
	"github.com/guregu/null"
	"github.com/vietta-net/agokit/auth"
	"github.com/vietta-net/agokit/errors"
	"github.com/vietta-net/agokit/helper"
	"github.com/vietta-net/agokit/sql"
	"google.golang.org/grpc/codes"
	"reflect"
	"time"

	"masterdata/internal/pkg/model"
	"masterdata/pkg/pb"
)

func (r repository)Get(id string)(*model.Data, error){
	m := &model.Data{}
	r.db.Where("id = ?", id).First(&m)
	if m.UpdatedAt.IsZero()  {
		return m, errors.E(
			codes.NotFound,
			fmt.Sprintf("Not found with ID `%s`", id),
		)
	}
	return m, nil
}

func SetTimezone(object *model.Data,  timezone string)( err error){
	e := reflect.ValueOf(object).Elem()
	loc, err := time.LoadLocation(timezone)
	if err != nil {
		return err
	}
	var data = make(map[string]interface{})

	for i := 0; i < e.NumField(); i++ {
		varType := e.Type().Field(i).Type
		varName := e.Type().Field(i).Name
		varValue := e.Field(i).Interface()
		if varType.String() == "time.Time" {
			var t = varValue.(time.Time)
			data[varName] = t.In(loc)
		}else{
			data[varName] = e.Field(i).Interface()
		}
	}

	bytes, err := json.Marshal(data)

	if err != nil {
		return err
	}

	json.Unmarshal(bytes, &object)

	return nil
}

func (r repository) Create(data *pb.Data) (err error) {
	user := auth.LoadUserFromContext(r.ctx)

	m := &model.Data {
		Mode: 		1,
		Group:       data.Group,
		Type:        data.Type,
		Code:        data.Code,
		Name:        data.Name,
		Ordering:    data.Ordering,
		Status:      data.Status,
		CreatedBy: user.GetUserName(),
		Deletable: sql.BoolToUint32(true),
	}

	if data.Description != "" {
		m.Description = null.StringFrom(data.Description)
	}

	if data.Parent != "" {
		m.Parent = null.StringFrom(data.Parent)
	}

	err = m.Validate(model.Create)
	if err != nil {
		return err
	}

	err = r.db.Create(&m).Error
	if err != nil {
		return errors.E(codes.Unknown, err)
	}
	data.Id = m.ID
	return  nil
}



func (r repository) Read(id string) (data *pb.Data, err error) {
	data = &pb.Data{}

	m, err := r.Get(id)
	if err != nil {
		return  data, err
	}

	err = SetTimezone(m, r.GetTimezone())

	if err != nil {
		return  data, err
	}

	err = helper.Convert(m, &data)
	return data, err
}

func (r repository) Update(data *pb.Data) (err error) {
	m, err := r.Get(data.Id)
	if err != nil {
		return  err
	}

	m.Status = data.Status
	m.Name = data.Name
	if data.Description != "" {
		m.Description = null.StringFrom(data.Description)
	}
	err = m.Validate(model.Update)
	if err != nil {
		return err
	}

	err = r.db.Model(&m).UpdateColumns(m).Error
	if err != nil {
		return errors.E(codes.Unknown, err, "Update")
	}
	return nil
}

func (r repository) Delete(id string) (err error) {
	m, err := r.Get(id)
	if err != nil {
		return  err
	}

	if m.Deletable > 0 {
		err = r.db.Delete(&m).Error
	}

	if err != nil {
		err = errors.E(codes.Unknown, "Delete")
	}

	return err
}

func (r repository) Search(query *pb.Query, filter *pb.Filter) (data []*pb.Data, pagination *pb.Pagination, err error) {
	pagination = &pb.Pagination{}
	DB := r.db
	var results []*model.Data
	d := &model.Data{}

	resultOrm := DB.Model(d )

	resultOrm, err = sql.BuildWhereFilter(resultOrm, filter)
	if  err != nil {
		return data, pagination, err
	}

	if query != nil{
		for _, date := range query.Dates{
			resultOrm, err = sql.BuildWhereDateRange(resultOrm, date, r.GetTimezone())
			if  err != nil {
				return data, pagination, err
			}
		}
		//Search in multiple fields
		whereString, args := sql.BuildWhereSearch([]string{"name", "description"}, query.Keyword)
		resultOrm = resultOrm.Where(whereString, args...)

		if query.Limit > 0 {
			//Count total rows and calculate total pages
			pagi, err := sql.GetPagination(resultOrm, query.Page, query.Limit)
			if  err != nil {
				err = errors.E(codes.Unknown, "GetPagination", err)
				return data, pagination, err
			}


			err = helper.Convert(pagi, &pagination)
			if  err != nil {
				err = errors.E(codes.Unknown, "Convert", err)
				return data, pagination, err
			}

			if pagination.Count == 0 {
				err = errors.E(codes.NotFound, "Not Found")
				return data, pagination, err
			}
			//Set limit
			if query.Page > 0 {
				offset := (query.Page - 1) * query.Limit
				resultOrm = resultOrm.Offset(offset).Limit(query.Limit)
			} else {
				resultOrm = resultOrm.Limit(query.Limit)
			}
		}
		//Set Order
		if query.Order != "" {
			resultOrm = resultOrm.Order(query.Order)
		}
	}

	if err = resultOrm.Find(&results).Error; err != nil {
		err = errors.E(codes.Unknown, "Convert", err)
		return data, pagination, err
	}

	if len(results) == 0 {
		err = errors.E(codes.NotFound, "Not Found")
		return data, pagination, err
	}

	//Convert to proto Data
	for _, m := range results {
		var v = pb.Data{}
		err = SetTimezone(m, r.GetTimezone())

		err = helper.Convert(m, &v)
		if  err != nil {
			err = errors.E(codes.Unknown, "Convert", err)
			return data, pagination, err
		}
		data = append(data, &v)
	}
	return data, pagination, err
}
