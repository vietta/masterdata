package service

import (
	"context"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/vietta-net/agokit/config"
	"github.com/vietta-net/agokit/gokit"
	"masterdata/internal/pkg/model"
	"masterdata/pkg/pb"
)

// Repository describes the persistence on order model
type Repository interface {
	Create(data *pb.Data) (err error)
	Read(id string) (data *pb.Data, err error)
	Update(data *pb.Data) (err error)
	Delete(id string) (err error)
	Search(query *pb.Query, filter *pb.Filter) (results []*pb.Data, pagination *pb.Pagination , err error)
}

type repository struct {
	db *gorm.DB
	mws *config.Middlewares
	ctx context.Context
}

func NewRepository(ctx context.Context, db *gorm.DB, mws *config.Middlewares) Repository {

	var repo = repository{db: db, mws:mws,ctx:ctx}
	model.Language = repo.GetLanguage()
	model.Timezone = repo.GetTimezone()

	return &repo
}

func (r repository) GetTimezone() string  {
	return gokit.GetTimezoneFromContext(r.ctx)
}

func (r repository) GetLanguage() string  {
	return gokit.GetLanguageFromContext(r.ctx)
}



