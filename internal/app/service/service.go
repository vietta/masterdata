package service

import (
	"context"
	"github.com/jinzhu/gorm"
	config "github.com/vietta-net/agokit/config"
	"github.com/vietta-net/agokit/errors"
	"github.com/vietta-net/agokit/i18n"

	"google.golang.org/grpc/codes"
	pb "masterdata/pkg/pb"
	"masterdata/pkg/service"
)

const (
	// apiVersion is version of API is provided by server
	apiVersion = "v1"
)

// NewmasterdataService returns a naïve, stateless implementation of Service.
func NewmasterdataService(db *gorm.DB, mws *config.Middlewares) service.Service {
	return &masterdataService{db: db, i18n: mws.Locale, mws:mws}
}

type masterdataService struct {
    db *gorm.DB
    i18n i18n.Locale
	mws *config.Middlewares
}

// checkAPI checks if the API version requested by client is supported by server
func (s *masterdataService) checkAPI(api string) (err error) {
	// API version is "" means use current version of the service
	if len(api) > 0 && apiVersion != api {
		message := &i18n.Message{
			Other: UnsupportedAPI,
		}
		params := map[string]string{
			"ServerApi": apiVersion,
			"ClientApi": api,
		}
		msg := s.i18n.T("Api", message, params)
		return errors.E(codes.Unimplemented, msg)
	}
	return nil
}

// New returns a basic Service with all of the expected middlewares wired in.
func New(cfg *config.BasicConfig) service.Service {
	var svc service.Service
	{
		svc = NewmasterdataService(cfg.Bb, &cfg.Mws)
		svc = service.LoggingMiddleware(cfg.Mws.Logger)(svc)
		svc = service.InstrumentingMiddleware(&cfg.Mws)(svc)
	}
	return svc
}


  
func (s *masterdataService) Create(ctx context.Context, req *pb.CreateRequest) (res *pb.CreateResponse, err error) {
	res = &pb.CreateResponse{
		Status: &pb.Error{
			Message: "Created",
			Code: uint32(codes.OK),
		},
	}

	if err = s.checkAPI(req.Api); err != nil {
		var e = err.(*errors.Error)
		res.Status.Code = e.Code
		res.Status.Message = e.Message
		return res, err
	}

    repo := NewRepository(ctx, s.db, s.mws)
	err = repo.Create(req.Data)

	return res, err
}

func (s *masterdataService) Read(ctx context.Context, req *pb.ReadRequest) (res *pb.ReadResponse, err error) {
	res = &pb.ReadResponse{
		Status: &pb.Error{
			Message: "Read",
			Code: uint32(codes.OK),
		},
	}

	if err = s.checkAPI(req.Api); err != nil {
		var e = err.(*errors.Error)
		res.Status.Code = e.Code
		res.Status.Message = e.Message
		return res, err
	}

	repo := NewRepository(ctx, s.db, s.mws)
	res.Data, err = repo.Read(req.Id)

	return res, err
}

func (s *masterdataService) Update(ctx context.Context, req *pb.UpdateRequest) (res *pb.UpdateResponse, err error) {
	res = &pb.UpdateResponse{
		Status: &pb.Error{
			Message: "Updated",
			Code: uint32(codes.OK),
		},
	}

	if err = s.checkAPI(req.Api); err != nil {
		var e = err.(*errors.Error)
		res.Status.Code = e.Code
		res.Status.Message = e.Message
		return res, err
	}

	repo := NewRepository(ctx, s.db, s.mws)
	err = repo.Update(req.Data)

	return res, err
}

func (s *masterdataService) Delete(ctx context.Context, req *pb.DeleteRequest) (res *pb.DeleteResponse, err error) {
	res = &pb.DeleteResponse{
		Status: &pb.Error{
			Message: "Deleted",
			Code: uint32(codes.OK),
		},
	}

	if err = s.checkAPI(req.Api); err != nil {
		var e = err.(*errors.Error)
		res.Status.Code = e.Code
		res.Status.Message = e.Message
		return res, err
	}

	repo := NewRepository(ctx, s.db, s.mws)
	err = repo.Delete(req.Id)

	return res, err
}

func (s *masterdataService) Search(ctx context.Context, req *pb.SearchRequest) (res *pb.SearchResponse, err error) {
	res = &pb.SearchResponse{
		Status: &pb.Error{
			Message: "Found",
			Code: uint32(codes.OK),
		},
	}

	if err = s.checkAPI(req.Api); err != nil {
		var e = err.(*errors.Error)
		res.Status.Code = e.Code
		res.Status.Message = e.Message
		return res, err
	}

	repo := NewRepository(ctx, s.db, s.mws)

	res.Data , res.Pagination, err = repo.Search(req.Query, req.Filter)
	return res, err
}  