package service_test


import (
	stdjwt "github.com/dgrijalva/jwt-go"
	"github.com/go-kit/kit/auth/jwt"
	"github.com/vietta-net/agokit/config"

	"google.golang.org/grpc/metadata"
	"log"
	"masterdata/pkg/pb"
	"masterdata/pkg/service"
	app "masterdata/internal/app/service"

	"os"
	"path/filepath"
	"testing"
	"context"
	"github.com/stretchr/testify/assert"
)

var _ = func() bool {
	testing.Init()
	return true
}()

var (
	cfg *config.BasicConfig
	err error
	Mws config.Middlewares
	svc service.Service
	ctx = context.Background()

)

func init(){
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	parent := filepath.Dir(path)
	parent = filepath.Dir(parent)
	parent = filepath.Dir(parent)

	var con config.Config
	con, err = config.New(parent)
	cfg = con.(*config.BasicConfig)
	Mws = cfg.LoadMiddlewares()
	svc    = app.New(cfg)
}



func SetContext()(ctx context.Context){

	tokenString := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJwbnRuNzkiLC" +
		"Jpc3MiOiJKb2huIERvZSIsImF1ZCI6IldlYiJ9.v0ftayqSMDefdanGepNm8aYWWI3JTJ5zyTsdrhguQKs"

	//https://tools.ietf.org/html/rfc4647
	md := metadata.Pairs(
		"content-language", "vi",
		"timezone", "Asia/Ho_Chi_Minh",
		"authorization" , "Bearer " + tokenString)
	ctx = metadata.NewOutgoingContext(ctx, md)

	kf := func(token *stdjwt.Token) (interface{}, error) { return []byte(cfg.App.Secret), nil }
	token, _ := stdjwt.ParseWithClaims(tokenString, jwt.StandardClaimsFactory(),kf)
	ctx 	= context.WithValue(ctx, jwt.JWTClaimsContextKey, token.Claims)

	return ctx
}

func TestCheckAPI(t *testing.T) {

	req := &pb.CreateRequest{
		Api: "v2",
		Data: &pb.Data{},
	}
	ctx := SetContext()
	_, err := svc.Create(ctx, req)
	assert.Error(t, err)
	//assert.Equal(t, uint32(codes.Unimplemented), err.(*errors.Error).Code)

}
