# Start from the latest alpine
FROM alpine:latest

ADD ./workdir/masterdata /masterdata
ADD ./configs /configs
ADD ./languages /languages

# Expose ports to the outside world
EXPOSE  64000 8082

# Command to run the executable
#CMD masterdata -config-path=/configs -language-path=/languages

ENTRYPOINT ["/masterdata"]