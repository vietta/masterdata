package main_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/vietta-net/agokit/config"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"log"
	grpcclient "masterdata/pkg/client/grpc"
	"masterdata/pkg/pb"
	"os"
	"path/filepath"
	"testing"
	"time"
)
const (
	// apiVersion is version of API is provided by server
	apiVersion = "v1"
	serverAddr = "127.0.0.1:64000"
)

func TestClient(t *testing.T) {
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	parent := filepath.Dir(filepath.Dir(path))

	// get configuration
	cfgs, err := config.New(parent)
	cfg := cfgs.(*config.BasicConfig)
	_ = cfg.LoadMiddlewares()

	conn, err := grpc.Dial(serverAddr, grpc.WithInsecure())

	c := grpcclient.NewGRPCClient(conn, cfg)

	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()


	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	data := pb.Data{}


	req := pb.CreateRequest{
		Api:        apiVersion,
		Data: &data,
	}

	res, err := c.Create(ctx, &req)

	if err != nil{
		assert.Error(t, err)
		t.Log(err.Error())
	}else {
		assert.Equal(t, uint32(codes.InvalidArgument), res.Status.Code)
		t.Log(res.Status.Message)
	}

}