package main

import (
	"context"
	"google.golang.org/grpc/codes"
	"masterdata/pkg/pb"
	"log"
	"os"
	"time"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

var (
	// apiVersion is version of API is provided by server
	apiVersion = "v1"
	serverAddr = "127.0.0.1:64000"
)


func main() {

	conn, err := grpc.Dial(serverAddr, grpc.WithInsecure())

	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := pb.NewMasterdataServiceClient(conn)

	jwtToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJwbnRuNzkiLC" +
		"Jpc3MiOiJKb2huIERvZSIsImF1ZCI6IldlYiJ9.v0ftayqSMDefdanGepNm8aYWWI3JTJ5zyTsdrhguQKs"


	//https://tools.ietf.org/html/rfc4647
	md := metadata.Pairs(
		"content-language", "vi",
		"timezone", "Asia/Ho_Chi_Minh",
		"authorization" , "Bearer " + jwtToken,)
	ctx := metadata.NewOutgoingContext(context.Background(), md)

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)


	defer cancel()

	data := pb.Data{}

	req := pb.CreateRequest{
		Api:        apiVersion,
		Data: &data,
	}

	res, err := c.Create(ctx, &req)

	if res != nil && res.Status.Code == uint32(codes.InvalidArgument) {
		//It works properly and exit
		os.Exit(0)
	}else{

		log.Printf(err.Error())
		os.Exit(1)
	}


}