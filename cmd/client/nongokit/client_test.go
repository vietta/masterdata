package main_test

import (
	"context"
	"github.com/stretchr/testify/assert"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"log"
	"masterdata/pkg/pb"
	"testing"
	"time"
)
const (
	// apiVersion is version of API is provided by server
	apiVersion = "v1"
	serverAddr = "127.0.0.1:64000"
)

func TestClient(t *testing.T) {

	conn, err := grpc.Dial(serverAddr, grpc.WithInsecure())

	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := pb.NewMasterdataServiceClient(conn)

	jwtToken := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJwbnRuNzkiLC" +
		"Jpc3MiOiJKb2huIERvZSIsImF1ZCI6IldlYiJ9.v0ftayqSMDefdanGepNm8aYWWI3JTJ5zyTsdrhguQKs"


	//https://tools.ietf.org/html/rfc4647
	md := metadata.Pairs(
		"content-language", "vi",
		"timezone", "Asia/Ho_Chi_Minh",
		"authorization" , "Bearer " + jwtToken,)
	ctx := metadata.NewOutgoingContext(context.Background(), md)

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)


	defer cancel()

	data := pb.Data{}

	req := pb.CreateRequest{
		Api:        apiVersion,
		Data: &data,
	}

	res, err := c.Create(ctx, &req)

	if err != nil{
		assert.Error(t, err)
		t.Log(err.Error())
	}else{
		assert.Equal(t, uint32(codes.InvalidArgument), res.Status.Code)
		t.Log(res.Status.Message)
	}

}