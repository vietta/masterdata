package main

import (
	"context"
	pb "masterdata/pkg/pb"
    grpcclient "masterdata/pkg/client/grpc"

	"log"
	"time"
    "github.com/vietta-net/agokit/config"
	"google.golang.org/grpc"
)

const (
	// apiVersion is version of API is provided by server
	apiVersion = "v1"
	serverAddr = "127.0.0.1:64000"
)

func main() {
	// get configuration
	cfgs, err := config.New("./")
    cfg := cfgs.(*config.BasicConfig)
    _ = cfg.LoadMiddlewares()

    conn, err := grpc.Dial(serverAddr, grpc.WithInsecure())

    c := grpcclient.NewGRPCClient(conn, cfg)

	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	//c := v1.NewMasterdataServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	data := pb.Data{}


	req := pb.CreateRequest{
		Api:        apiVersion,
		Data: &data,
	}

	res, err := c.Create(ctx, &req)

	if err != nil {
		log.Fatalf("Create failed: %v", err)
	}

	log.Printf("Result: Message: %s\n",res.Status.Message)

}
