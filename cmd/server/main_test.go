package main_test

import (
	"google.golang.org/grpc/codes"
	"log"
	"masterdata/internal/app/service"
	"os"
	"path/filepath"
	"testing"
	"time"
)



func TestServer(t *testing.T) {
	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	parent := filepath.Dir(filepath.Dir(path))
	ExitServer(30 )
	go service.Run(parent)
}

func ExitServer(timeout time.Duration) {
	go func() {
		time.Sleep(timeout * time.Second)
		os.Exit(int(codes.OK))
	}()
}