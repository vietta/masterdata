package main

import (
	"masterdata/internal/app/service"
	"fmt"
	"os"
)

func main() {
	if err := service.Run("./"); err != nil {
    		fmt.Fprintf(os.Stderr, "%v\n", err)
    		os.Exit(1)
    }
}