#!/bin/sh
cd ..
cd ..

GITURL=`git config --get remote.origin.url`
VERSION=`git ls-remote --tags --refs --sort="version:refname" $GITURL | awk -F/ 'END{print$NF}'`
VERSION=${VERSION:-'0.1.0'}
MAJOR="${VERSION%%.*}"; VERSION="${VERSION#*.}"
MINOR="${VERSION%%.*}"; VERSION="${VERSION#*.}"
PATCH="${VERSION%%.*}"; VERSION="${VERSION#*.}"
PATCH=$((PATCH+1))
TAG="$MAJOR.$MINOR.$PATCH"

git config --global user.email "$EMAIL"
git config --global user.name "$AUTHOR"

GIT_COMMIT=`git rev-parse HEAD`
NEEDS_TAG=`git describe --contains $GIT_COMMIT`

if [ -z "$NEEDS_TAG" ]; then
  docker build -t pntn79/masterdata:qa-$TAG .
  docker build -t pntn79/masterdata:qa-latest .

  docker run -p 8082:8082 -p 64000:64000 --name masterdata -d pntn79/masterdata:qa-latest

  echo "$DOCKER_PASS" | docker login --username $DOCKER_USER --password-stdin
  docker push pntn79/masterdata:qa-$TAG
  docker push pntn79/masterdata:qa-latest

  git checkout -b staging
  git merge qa
  git push origin staging

else
  VERSION="$MAJOR.$MINOR.$PATCH"
  echo "Already version $VERSION on this commit"
fi
