#!/bin/sh

RESULT=`docker exec -it masterdata nc -vz 127.0.0.1 8080`

if [[ "$RESULT" == *"127.0.0.1"* ]]; then
    echo "Service is running" && exit 0
else
     echo "Service is not running" && exit 1
fi