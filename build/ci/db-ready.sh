#!/bin/sh

#Wait for MYSQL to be ready
for i in `seq 1 10`;
do
  nc -z 127.0.0.1 3306 && echo Success && exit 0
  echo -n .
  sleep 1
done
echo Failed waiting for MySQL && exit 1