#!/bin/sh
cd ..
cd ..

GITURL=`git config --get remote.origin.url`
VERSION=`git ls-remote --tags --refs --sort="version:refname" $GITURL | awk -F/ 'END{print$NF}'`
VERSION=${VERSION:-'0.1.0'}
MAJOR="${VERSION%%.*}"; VERSION="${VERSION#*.}"
MINOR="${VERSION%%.*}"; VERSION="${VERSION#*.}"
PATCH="${VERSION%%.*}"; VERSION="${VERSION#*.}"
PATCH=$((PATCH+1))
TAG="$MAJOR.$MINOR.$PATCH"

git config --global user.email "$EMAIL"
git config --global user.name "$AUTHOR"

GIT_COMMIT=`git rev-parse HEAD`
NEEDS_TAG=`git describe --contains $GIT_COMMIT`

if [ -z "$NEEDS_TAG" ]; then
  docker build -t pntn79/masterdata:$TAG .
  docker build -t pntn79/masterdata:latest .

  docker run -p 8082:8082 -p 64000:64000 --name masterdata -d pntn79/masterdata:latest
  #Circle CI does not work for these commands and check it later
  #docker exec -it masterdata apk add curl
  #docker exec -it masterdata curl --retry 10 --retry-connrefused http://localhost:8082/metrics

  echo "$DOCKER_PASS" | docker login --username $DOCKER_USER --password-stdin
  docker push pntn79/masterdata:$TAG
  docker push pntn79/masterdata:latest

  git checkout -b "release/$TAG"
  git push origin "release/$TAG"

  git tag "$TAG" -m "Release $TAG"
  echo "Updating to $TAG"
  git push origin "$TAG"
else
  VERSION="$MAJOR.$MINOR.$PATCH"
  echo "Already version $VERSION on this commit"
fi
