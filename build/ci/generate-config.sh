#! /bin/bash

cd ..
cd ..


DBCONFIG="development:
  dialect: mysql
  datasource: $DB_USER:$DB_PASSWORD@tcp($DB_HOST:$DB_PORT)/$DB_DATABASE?parseTime=true&charset=utf8mb4
  dir: migrations/mysql
  table: migrations"

echo $DBCONFIG > ./db/dbconfig.yml
