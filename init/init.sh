#! /bin/bash

cd ..

go get -u github.com/golang/protobuf/{proto,protoc-gen-go}
go get -u github.com/smallnest/gen
go get -u github.com/nicksnyder/go-i18n/v2/goi18n
go get -v github.com/rubenv/sql-migrate/...

go mod init masterdata;
go mod download
go mod vendor

