package endpoints

import (
     "context"
       "fmt"
       stdjwt "github.com/dgrijalva/jwt-go"
       "github.com/go-kit/kit/auth/jwt"
       "github.com/go-kit/kit/endpoint"
       config "github.com/vietta-net/agokit/config"
       "github.com/vietta-net/agokit/errors"

       pb "masterdata/pkg/pb"
)

var _ = endpoint.Chain
var _ = fmt.Errorf
var _ = context.Background

type StreamEndpoint func(server interface{}, req interface{}) (err error)

type Endpoints struct { 
     CreateEndpoint endpoint.Endpoint  
     ReadEndpoint endpoint.Endpoint  
     UpdateEndpoint endpoint.Endpoint  
     DeleteEndpoint endpoint.Endpoint  
     SearchEndpoint endpoint.Endpoint  
}

  
func (e *Endpoints) Create(ctx context.Context, in *pb.CreateRequest) (*pb.CreateResponse, error) {
    out, err := e.CreateEndpoint(ctx, in)
    if  out != nil {
        return out.(*pb.CreateResponse), err
    }
    return nil, err
}     
func (e *Endpoints) Read(ctx context.Context, in *pb.ReadRequest) (*pb.ReadResponse, error) {
    out, err := e.ReadEndpoint(ctx, in)
    if  out != nil {
        return out.(*pb.ReadResponse), err
    }
    return nil, err
}     
func (e *Endpoints) Update(ctx context.Context, in *pb.UpdateRequest) (*pb.UpdateResponse, error) {
    out, err := e.UpdateEndpoint(ctx, in)
    if  out != nil {
        return out.(*pb.UpdateResponse), err
    }
    return nil, err
}     
func (e *Endpoints) Delete(ctx context.Context, in *pb.DeleteRequest) (*pb.DeleteResponse, error) {
    out, err := e.DeleteEndpoint(ctx, in)
    if  out != nil {
        return out.(*pb.DeleteResponse), err
    }
    return nil, err
}     
func (e *Endpoints) Search(ctx context.Context, in *pb.SearchRequest) (*pb.SearchResponse, error) {
    out, err := e.SearchEndpoint(ctx, in)
    if  out != nil {
        return out.(*pb.SearchResponse), err
    }
    return nil, err
}   
 
func MakeCreateEndpoint(svc pb.MasterdataServiceServer) endpoint.Endpoint {
    return func(ctx context.Context, request interface{}) (interface{}, error) {
        req := request.(*pb.CreateRequest)
        res, err := svc.Create(ctx, req)
        if err != nil {
            status := err.(*errors.Error)
            if  status.Code > 0 {
                status.To(res.Status)
                res.Status.Stack = nil
                err =  nil
            }
        }
        return res, err
    }
}  
 
func MakeReadEndpoint(svc pb.MasterdataServiceServer) endpoint.Endpoint {
    return func(ctx context.Context, request interface{}) (interface{}, error) {
        req := request.(*pb.ReadRequest)
        res, err := svc.Read(ctx, req)
        if err != nil {
            status := err.(*errors.Error)
            if  status.Code > 0 {
                status.To(res.Status)
                res.Status.Stack = nil
                err =  nil
            }
        }
        return res, err
    }
}  
 
func MakeUpdateEndpoint(svc pb.MasterdataServiceServer) endpoint.Endpoint {
    return func(ctx context.Context, request interface{}) (interface{}, error) {
        req := request.(*pb.UpdateRequest)
        res, err := svc.Update(ctx, req)
        if err != nil {
            status := err.(*errors.Error)
            if  status.Code > 0 {
                status.To(res.Status)
                res.Status.Stack = nil
                err =  nil
            }
        }
        return res, err
    }
}  
 
func MakeDeleteEndpoint(svc pb.MasterdataServiceServer) endpoint.Endpoint {
    return func(ctx context.Context, request interface{}) (interface{}, error) {
        req := request.(*pb.DeleteRequest)
        res, err := svc.Delete(ctx, req)
        if err != nil {
            status := err.(*errors.Error)
            if  status.Code > 0 {
                status.To(res.Status)
                res.Status.Stack = nil
                err =  nil
            }
        }
        return res, err
    }
}  
 
func MakeSearchEndpoint(svc pb.MasterdataServiceServer) endpoint.Endpoint {
    return func(ctx context.Context, request interface{}) (interface{}, error) {
        req := request.(*pb.SearchRequest)
        res, err := svc.Search(ctx, req)
        if err != nil {
            status := err.(*errors.Error)
            if  status.Code > 0 {
                status.To(res.Status)
                res.Status.Stack = nil
                err =  nil
            }
        }
        return res, err
    }
}  


func MakeEndpoints(svc pb.MasterdataServiceServer , m *config.BasicConfig) Endpoints {
	
	var createEndpoint endpoint.Endpoint
    {
        kf := func(token *stdjwt.Token) (interface{}, error) { return []byte(m.App.Secret), nil }
        createEndpoint = MakeCreateEndpoint(svc)
        createEndpoint = jwt.NewParser(kf, stdjwt.SigningMethodHS256, jwt.StandardClaimsFactory)(createEndpoint)
    }
    
	var readEndpoint endpoint.Endpoint
    {
        kf := func(token *stdjwt.Token) (interface{}, error) { return []byte(m.App.Secret), nil }
        readEndpoint = MakeReadEndpoint(svc)
        readEndpoint = jwt.NewParser(kf, stdjwt.SigningMethodHS256, jwt.StandardClaimsFactory)(readEndpoint)
    }
    
	var updateEndpoint endpoint.Endpoint
    {
        kf := func(token *stdjwt.Token) (interface{}, error) { return []byte(m.App.Secret), nil }
        updateEndpoint = MakeUpdateEndpoint(svc)
        updateEndpoint = jwt.NewParser(kf, stdjwt.SigningMethodHS256, jwt.StandardClaimsFactory)(updateEndpoint)
    }
    
	var deleteEndpoint endpoint.Endpoint
    {
        kf := func(token *stdjwt.Token) (interface{}, error) { return []byte(m.App.Secret), nil }
        deleteEndpoint = MakeDeleteEndpoint(svc)
        deleteEndpoint = jwt.NewParser(kf, stdjwt.SigningMethodHS256, jwt.StandardClaimsFactory)(deleteEndpoint)
    }
    
	var searchEndpoint endpoint.Endpoint
    {
        kf := func(token *stdjwt.Token) (interface{}, error) { return []byte(m.App.Secret), nil }
        searchEndpoint = MakeSearchEndpoint(svc)
        searchEndpoint = jwt.NewParser(kf, stdjwt.SigningMethodHS256, jwt.StandardClaimsFactory)(searchEndpoint)
    }
    

	return Endpoints{ 
		CreateEndpoint: createEndpoint, 
		ReadEndpoint: readEndpoint, 
		UpdateEndpoint: updateEndpoint, 
		DeleteEndpoint: deleteEndpoint, 
		SearchEndpoint: searchEndpoint, 
	}
}
