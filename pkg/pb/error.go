package pb

import (
	"github.com/vietta-net/agokit/helper"
)

var (
	_ helper.Copy = &Error{}
)

func (x *Error) To(data interface{}) (err error)  {
	err = helper.Convert(x, &data)
	return err
}

func (x *Error) From(data interface{}) (err error)  {
	err = helper.Convert(data, &x)
	return err
}