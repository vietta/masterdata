package grpcclient

import (
	context "context"
	"fmt"
	stdjwt "github.com/dgrijalva/jwt-go"
	"github.com/go-kit/kit/auth/jwt"
	"github.com/go-kit/kit/endpoint"
	grpctransport "github.com/go-kit/kit/transport/grpc"
	"github.com/vietta-net/agokit/config"
	"github.com/vietta-net/agokit/i18n"
	"google.golang.org/grpc"
	pb "masterdata/pkg/pb"
	"time"
	"masterdata/pkg/endpoints"
	"masterdata/pkg/service"
	"github.com/go-kit/kit/circuitbreaker"
	"github.com/go-kit/kit/ratelimit"
	"github.com/go-kit/kit/tracing/zipkin"
	"github.com/sony/gobreaker"
	"golang.org/x/time/rate"
)
const serviceName = "masterdata"

func NewGRPCClient(conn *grpc.ClientConn, cfg *config.BasicConfig) service.Service {
	//otTracer stdopentracing.Tracer, zipkinTracer *stdzipkin.Tracer, logger log.Logger, jwtSigning string, env string, lang string
    // We construct a single ratelimiter middleware, to limit the total outgoing
    // QPS from this client to all methods on the remote instance. We also
    // construct per-endpoint circuitbreaker middlewares to demonstrate how
    // that's done, although they could easily be combined into a single breaker
    // for the entire remote instance, too.
    limiter := ratelimit.NewErroringLimiter(rate.NewLimiter(rate.Every(time.Second), 100))

    // global client middlewares
    var options []grpctransport.ClientOption

    if cfg.Mws.ZipkinTracer != nil {
        // Zipkin GRPC Client Trace can either be instantiated per gRPC method with a
        // provided operation name or a global tracing client can be instantiated
        // without an operation name and fed to each Go kit client as ClientOption.
        // In the latter case, the operation name will be the endpoint's grpc method
        // path.
        //
        // In this example, we demonstrace a global tracing client.
        options = append(options, zipkin.GRPCClientTrace(cfg.Mws.ZipkinTracer))

    }

    options = append(options, grpctransport.ClientBefore(i18n.GRPCClientlanguage(cfg.App.Language)))

	 
	var createEndpoint endpoint.Endpoint
	{
		createEndpoint = grpctransport.NewClient(
		conn,
		fmt.Sprintf("%s.MasterdataService", serviceName),
		"Create",
		EncodeCreateRequest,
		DecodeCreateResponse,
		pb.CreateResponse{},
		append(options, grpctransport.ClientBefore(jwt.ContextToGRPC()))...,
	).Endpoint()

		createEndpoint  = jwt.NewSigner(
		"kid-header",
		[]byte(cfg.App.Secret),
		stdjwt.SigningMethodHS256,
		stdjwt.StandardClaims{},
	)(createEndpoint)

		createEndpoint = TraceClient(cfg.Mws.Tracer, "Create", cfg.App.Env)(createEndpoint)
		createEndpoint = limiter(createEndpoint)
		createEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
		Name:    "Create",
		Timeout: 30 * time.Second,
	}))(createEndpoint)
	}   
	var readEndpoint endpoint.Endpoint
	{
		readEndpoint = grpctransport.NewClient(
		conn,
		fmt.Sprintf("%s.MasterdataService", serviceName),
		"Read",
		EncodeReadRequest,
		DecodeReadResponse,
		pb.ReadResponse{},
		append(options, grpctransport.ClientBefore(jwt.ContextToGRPC()))...,
	).Endpoint()

		readEndpoint  = jwt.NewSigner(
		"kid-header",
		[]byte(cfg.App.Secret),
		stdjwt.SigningMethodHS256,
		stdjwt.StandardClaims{},
	)(readEndpoint)

		readEndpoint = TraceClient(cfg.Mws.Tracer, "Create", cfg.App.Env)(readEndpoint)
		readEndpoint = limiter(createEndpoint)
		readEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
		Name:    "Create",
		Timeout: 30 * time.Second,
	}))(readEndpoint)
	}   
	var updateEndpoint endpoint.Endpoint
	{
		updateEndpoint = grpctransport.NewClient(
		conn,
		fmt.Sprintf("%s.MasterdataService", serviceName),
		"Update",
		EncodeUpdateRequest,
		DecodeUpdateResponse,
		pb.UpdateResponse{},
		append(options, grpctransport.ClientBefore(jwt.ContextToGRPC()))...,
	).Endpoint()

		updateEndpoint  = jwt.NewSigner(
		"kid-header",
		[]byte(cfg.App.Secret),
		stdjwt.SigningMethodHS256,
		stdjwt.StandardClaims{},
	)(updateEndpoint)

		updateEndpoint = TraceClient(cfg.Mws.Tracer, "Create", cfg.App.Env)(updateEndpoint)
		updateEndpoint = limiter(createEndpoint)
		updateEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
		Name:    "Create",
		Timeout: 30 * time.Second,
	}))(updateEndpoint)
	}   
	var deleteEndpoint endpoint.Endpoint
	{
		deleteEndpoint = grpctransport.NewClient(
		conn,
		fmt.Sprintf("%s.MasterdataService", serviceName),
		"Delete",
		EncodeDeleteRequest,
		DecodeDeleteResponse,
		pb.DeleteResponse{},
		append(options, grpctransport.ClientBefore(jwt.ContextToGRPC()))...,
	).Endpoint()

		deleteEndpoint  = jwt.NewSigner(
		"kid-header",
		[]byte(cfg.App.Secret),
		stdjwt.SigningMethodHS256,
		stdjwt.StandardClaims{},
	)(deleteEndpoint)

		deleteEndpoint = TraceClient(cfg.Mws.Tracer, "Create", cfg.App.Env)(deleteEndpoint)
		deleteEndpoint = limiter(createEndpoint)
		deleteEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
		Name:    "Create",
		Timeout: 30 * time.Second,
	}))(deleteEndpoint)
	}   
	var searchEndpoint endpoint.Endpoint
	{
		searchEndpoint = grpctransport.NewClient(
		conn,
		fmt.Sprintf("%s.MasterdataService", serviceName),
		"Search",
		EncodeSearchRequest,
		DecodeSearchResponse,
		pb.SearchResponse{},
		append(options, grpctransport.ClientBefore(jwt.ContextToGRPC()))...,
	).Endpoint()

		searchEndpoint  = jwt.NewSigner(
		"kid-header",
		[]byte(cfg.App.Secret),
		stdjwt.SigningMethodHS256,
		stdjwt.StandardClaims{},
	)(searchEndpoint)

		searchEndpoint = TraceClient(cfg.Mws.Tracer, "Create", cfg.App.Env)(searchEndpoint)
		searchEndpoint = limiter(createEndpoint)
		searchEndpoint = circuitbreaker.Gobreaker(gobreaker.NewCircuitBreaker(gobreaker.Settings{
		Name:    "Create",
		Timeout: 30 * time.Second,
	}))(searchEndpoint)
	}  

	return &endpoints.Endpoints { 
		
		CreateEndpoint: createEndpoint,   
		
		ReadEndpoint: readEndpoint,   
		
		UpdateEndpoint: updateEndpoint,   
		
		DeleteEndpoint: deleteEndpoint,   
		
		SearchEndpoint: searchEndpoint,   
	}
}


 
func EncodeCreateRequest(_ context.Context, request interface{}) (interface{}, error) {
    req := request.(*pb.CreateRequest)
    return req, nil
}

func DecodeCreateResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
    response := grpcResponse.(*pb.CreateResponse)
    return response, nil
}   
func EncodeReadRequest(_ context.Context, request interface{}) (interface{}, error) {
    req := request.(*pb.ReadRequest)
    return req, nil
}

func DecodeReadResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
    response := grpcResponse.(*pb.ReadResponse)
    return response, nil
}   
func EncodeUpdateRequest(_ context.Context, request interface{}) (interface{}, error) {
    req := request.(*pb.UpdateRequest)
    return req, nil
}

func DecodeUpdateResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
    response := grpcResponse.(*pb.UpdateResponse)
    return response, nil
}   
func EncodeDeleteRequest(_ context.Context, request interface{}) (interface{}, error) {
    req := request.(*pb.DeleteRequest)
    return req, nil
}

func DecodeDeleteResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
    response := grpcResponse.(*pb.DeleteResponse)
    return response, nil
}   
func EncodeSearchRequest(_ context.Context, request interface{}) (interface{}, error) {
    req := request.(*pb.SearchRequest)
    return req, nil
}

func DecodeSearchResponse(_ context.Context, grpcResponse interface{}) (interface{}, error) {
    response := grpcResponse.(*pb.SearchResponse)
    return response, nil
}  