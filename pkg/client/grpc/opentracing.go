package grpcclient

import (
	"context"
	"github.com/opentracing/opentracing-go"
	"github.com/go-kit/kit/endpoint"
)
func TraceClient(tracer opentracing.Tracer, operationName string, env string) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (interface{}, error) {
			var span opentracing.Span
			span = tracer.StartSpan(operationName)

			span.SetTag("kind", "client")

			span.SetTag("environment", env)

			defer span.Finish()
			ctx = opentracing.ContextWithSpan(ctx, span)
			return next(ctx, request)
		}
	}
}
