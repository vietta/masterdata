package service

import (
	"context"
	pb "masterdata/pkg/pb"
)

type Service interface {   
     Create(ctx context.Context, req *pb.CreateRequest) (*pb.CreateResponse, error)     
     Read(ctx context.Context, req *pb.ReadRequest) (*pb.ReadResponse, error)     
     Update(ctx context.Context, req *pb.UpdateRequest) (*pb.UpdateResponse, error)     
     Delete(ctx context.Context, req *pb.DeleteRequest) (*pb.DeleteResponse, error)     
     Search(ctx context.Context, req *pb.SearchRequest) (*pb.SearchResponse, error)   
}

