package service

import (
	"context"

	pb "masterdata/pkg/pb"
	"time"

	"github.com/go-kit/kit/log"

)

// LoggingMiddleware takes a logger as a dependency
// and returns a service Middleware.
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next Service) Service {
		return &loggingMiddleware{logger, next}
	}
}

type loggingMiddleware struct {
	logger log.Logger
	next   Service
}
 
func (mw loggingMiddleware) Create(ctx context.Context, req *pb.CreateRequest) (res *pb.CreateResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
            mw.logger.Log(
                "method", "Create",
                "api", req.Api,
                "error", err.Error(),
                "took", time.Since(begin),
            )
        }else{
            mw.logger.Log(
                "method", "Create",
                "api", req.Api,
                "status", res.Status.Message,
                "took", time.Since(begin),
            )
        }
		}(time.Now())
	res, err = mw.next.Create(ctx, req)
	return res, err
} 
func (mw loggingMiddleware) Read(ctx context.Context, req *pb.ReadRequest) (res *pb.ReadResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
            mw.logger.Log(
                "method", "Read",
                "api", req.Api,
                "error", err.Error(),
                "took", time.Since(begin),
            )
        }else{
            mw.logger.Log(
                "method", "Read",
                "api", req.Api,
                "status", res.Status.Message,
                "took", time.Since(begin),
            )
        }
		}(time.Now())
	res, err = mw.next.Read(ctx, req)
	return res, err
} 
func (mw loggingMiddleware) Update(ctx context.Context, req *pb.UpdateRequest) (res *pb.UpdateResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
            mw.logger.Log(
                "method", "Update",
                "api", req.Api,
                "error", err.Error(),
                "took", time.Since(begin),
            )
        }else{
            mw.logger.Log(
                "method", "Update",
                "api", req.Api,
                "status", res.Status.Message,
                "took", time.Since(begin),
            )
        }
		}(time.Now())
	res, err = mw.next.Update(ctx, req)
	return res, err
} 
func (mw loggingMiddleware) Delete(ctx context.Context, req *pb.DeleteRequest) (res *pb.DeleteResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
            mw.logger.Log(
                "method", "Delete",
                "api", req.Api,
                "error", err.Error(),
                "took", time.Since(begin),
            )
        }else{
            mw.logger.Log(
                "method", "Delete",
                "api", req.Api,
                "status", res.Status.Message,
                "took", time.Since(begin),
            )
        }
		}(time.Now())
	res, err = mw.next.Delete(ctx, req)
	return res, err
} 
func (mw loggingMiddleware) Search(ctx context.Context, req *pb.SearchRequest) (res *pb.SearchResponse, err error) {
	defer func(begin time.Time) {
		if err != nil {
            mw.logger.Log(
                "method", "Search",
                "api", req.Api,
                "error", err.Error(),
                "took", time.Since(begin),
            )
        }else{
            mw.logger.Log(
                "method", "Search",
                "api", req.Api,
                "status", res.Status.Message,
                "took", time.Since(begin),
            )
        }
		}(time.Now())
	res, err = mw.next.Search(ctx, req)
	return res, err
}