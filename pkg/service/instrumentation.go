package service

import (
	"context"
	"fmt"
	pb "masterdata/pkg/pb"
    config "github.com/vietta-net/agokit/config"
	"github.com/go-kit/kit/metrics"
	"time"
)

// InstrumentingMiddleware returns a service middleware that instruments
// the number of integers summed and characters concatenated over the lifetime of
// the service.
func InstrumentingMiddleware(mws *config.Middlewares) Middleware {
	return func(next Service) Service {
		return instrumentingMiddleware{
			requestCount:   mws.RequestCount,
			requestLatency: mws.RequestLatency,
			countResult:    mws.CountResult,
			next:           next,
		}
	}
}

type instrumentingMiddleware struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	countResult    metrics.Histogram
	next           Service
}
 
func (mw instrumentingMiddleware) Create(ctx context.Context, req *pb.CreateRequest) (res *pb.CreateResponse, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "Create", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	res, err = mw.next.Create(ctx, req)
	return res, err
} 
func (mw instrumentingMiddleware) Read(ctx context.Context, req *pb.ReadRequest) (res *pb.ReadResponse, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "Read", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	res, err = mw.next.Read(ctx, req)
	return res, err
} 
func (mw instrumentingMiddleware) Update(ctx context.Context, req *pb.UpdateRequest) (res *pb.UpdateResponse, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "Update", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	res, err = mw.next.Update(ctx, req)
	return res, err
} 
func (mw instrumentingMiddleware) Delete(ctx context.Context, req *pb.DeleteRequest) (res *pb.DeleteResponse, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "Delete", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	res, err = mw.next.Delete(ctx, req)
	return res, err
} 
func (mw instrumentingMiddleware) Search(ctx context.Context, req *pb.SearchRequest) (res *pb.SearchResponse, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "Search", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	res, err = mw.next.Search(ctx, req)
	return res, err
}