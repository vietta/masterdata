package grpctransports

import (
     "context"
    "github.com/vietta-net/agokit/config"
    "github.com/vietta-net/agokit/i18n"
     "github.com/vietta-net/agokit/gokit"
    pb "masterdata/pkg/pb"
    "fmt"
    "github.com/go-kit/kit/auth/jwt"
    grpc "github.com/go-kit/kit/transport/grpc"
    "github.com/go-kit/kit/tracing/zipkin"
    "github.com/go-kit/kit/transport"
    grpctransport "github.com/go-kit/kit/transport/grpc"
    "masterdata/pkg/endpoints"
)

// avoid import errors
var _ = fmt.Errorf

func MakeGRPCServer(endpoints endpoints.Endpoints, cfg *config.BasicConfig) pb.MasterdataServiceServer {

//i18N i18n.I18N,
    //otTracer stdopentracing.Tracer, zipkinTracer *stdzipkin.Tracer, logger log.Logger, env string
    options := []grpctransport.ServerOption{
        grpctransport.ServerErrorHandler(transport.NewLogErrorHandler(cfg.Mws.Logger)),
    }

    if cfg.Mws.ZipkinTracer != nil {
        options = append(options, zipkin.GRPCServerTrace(cfg.Mws.ZipkinTracer))
    }

    options= append(options, grpc.ServerBefore(jwt.GRPCToContext()))
    options= append(options, grpc.ServerBefore(i18n.LanguageToContext(cfg.Mws.Locale)))
    options= append(options, grpc.ServerBefore(gokit.TimezoneToContext(cfg.App.Timezone)))


	return &grpcServer{   
        create: grpc.NewServer(
            endpoints.CreateEndpoint,
            decodeRequest,
            encodeCreateResponse,
            append(options, grpctransport.ServerBefore(GRPCToContext(cfg.Mws.Tracer, "Create", cfg.Mws.Logger, cfg.App.Env)))...,

        ),    
        read: grpc.NewServer(
            endpoints.ReadEndpoint,
            decodeRequest,
            encodeReadResponse,
            append(options, grpctransport.ServerBefore(GRPCToContext(cfg.Mws.Tracer, "Read", cfg.Mws.Logger, cfg.App.Env)))...,

        ),    
        update: grpc.NewServer(
            endpoints.UpdateEndpoint,
            decodeRequest,
            encodeUpdateResponse,
            append(options, grpctransport.ServerBefore(GRPCToContext(cfg.Mws.Tracer, "Update", cfg.Mws.Logger, cfg.App.Env)))...,

        ),    
        delete: grpc.NewServer(
            endpoints.DeleteEndpoint,
            decodeRequest,
            encodeDeleteResponse,
            append(options, grpctransport.ServerBefore(GRPCToContext(cfg.Mws.Tracer, "Delete", cfg.Mws.Logger, cfg.App.Env)))...,

        ),    
        search: grpc.NewServer(
            endpoints.SearchEndpoint,
            decodeRequest,
            encodeSearchResponse,
            append(options, grpctransport.ServerBefore(GRPCToContext(cfg.Mws.Tracer, "Search", cfg.Mws.Logger, cfg.App.Env)))...,

        ),  
	}
}

type grpcServer struct {  
    create grpc.Handler   
    read grpc.Handler   
    update grpc.Handler   
    delete grpc.Handler   
    search grpc.Handler  
}
 
func (s *grpcServer) Create(ctx context.Context, req *pb.CreateRequest) (*pb.CreateResponse, error) {
_, rep, err := s.create.ServeGRPC(ctx, req)
    if err != nil {
        return nil, err
    }
    return rep.(*pb.CreateResponse), nil
}

func encodeCreateResponse(ctx context.Context, response interface{}) (interface{}, error) {
    resp := response.(*pb.CreateResponse)
    return resp, nil
}   
func (s *grpcServer) Read(ctx context.Context, req *pb.ReadRequest) (*pb.ReadResponse, error) {
_, rep, err := s.read.ServeGRPC(ctx, req)
    if err != nil {
        return nil, err
    }
    return rep.(*pb.ReadResponse), nil
}

func encodeReadResponse(ctx context.Context, response interface{}) (interface{}, error) {
    resp := response.(*pb.ReadResponse)
    return resp, nil
}   
func (s *grpcServer) Update(ctx context.Context, req *pb.UpdateRequest) (*pb.UpdateResponse, error) {
_, rep, err := s.update.ServeGRPC(ctx, req)
    if err != nil {
        return nil, err
    }
    return rep.(*pb.UpdateResponse), nil
}

func encodeUpdateResponse(ctx context.Context, response interface{}) (interface{}, error) {
    resp := response.(*pb.UpdateResponse)
    return resp, nil
}   
func (s *grpcServer) Delete(ctx context.Context, req *pb.DeleteRequest) (*pb.DeleteResponse, error) {
_, rep, err := s.delete.ServeGRPC(ctx, req)
    if err != nil {
        return nil, err
    }
    return rep.(*pb.DeleteResponse), nil
}

func encodeDeleteResponse(ctx context.Context, response interface{}) (interface{}, error) {
    resp := response.(*pb.DeleteResponse)
    return resp, nil
}   
func (s *grpcServer) Search(ctx context.Context, req *pb.SearchRequest) (*pb.SearchResponse, error) {
_, rep, err := s.search.ServeGRPC(ctx, req)
    if err != nil {
        return nil, err
    }
    return rep.(*pb.SearchResponse), nil
}

func encodeSearchResponse(ctx context.Context, response interface{}) (interface{}, error) {
    resp := response.(*pb.SearchResponse)
    return resp, nil
}  


func decodeRequest(ctx context.Context, grpcReq interface{}) (interface{}, error) {
	return grpcReq, nil
}

type streamHandler interface{
	Do(server interface{}, req interface{}) (err error)
}

type server struct {
	e endpoints.StreamEndpoint
}

func (s server) Do(server interface{}, req interface{}) (err error) {
	if err := s.e(server, req); err != nil {
		return err
	}
	return nil
}
