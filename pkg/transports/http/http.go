package httpgrpctransports



import (
       "log"
	    "net/http"
	    "encoding/json"
	    "context"
        "github.com/go-kit/kit/endpoint"
        httptransport "github.com/go-kit/kit/transport/http"

        pb "masterdata/pkg/pb"
        "masterdata/pkg/endpoints"
)

var _ = log.Printf
var _ = endpoint.Chain
var _ = httptransport.NewClient
 
func MakeCreateHandler(svc pb.MasterdataServiceServer, endpoint endpoint.Endpoint) *httptransport.Server {
    return httptransport.NewServer(
        endpoint,
        decodeCreateRequest,
        encodeResponse,
        []httptransport.ServerOption{}...,
    )
}

func decodeCreateRequest(ctx context.Context, r *http.Request) (interface{}, error) {
    var req pb.CreateRequest
    if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
        return nil, err
    }
    return &req, nil
}   
func MakeReadHandler(svc pb.MasterdataServiceServer, endpoint endpoint.Endpoint) *httptransport.Server {
    return httptransport.NewServer(
        endpoint,
        decodeReadRequest,
        encodeResponse,
        []httptransport.ServerOption{}...,
    )
}

func decodeReadRequest(ctx context.Context, r *http.Request) (interface{}, error) {
    var req pb.ReadRequest
    if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
        return nil, err
    }
    return &req, nil
}   
func MakeUpdateHandler(svc pb.MasterdataServiceServer, endpoint endpoint.Endpoint) *httptransport.Server {
    return httptransport.NewServer(
        endpoint,
        decodeUpdateRequest,
        encodeResponse,
        []httptransport.ServerOption{}...,
    )
}

func decodeUpdateRequest(ctx context.Context, r *http.Request) (interface{}, error) {
    var req pb.UpdateRequest
    if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
        return nil, err
    }
    return &req, nil
}   
func MakeDeleteHandler(svc pb.MasterdataServiceServer, endpoint endpoint.Endpoint) *httptransport.Server {
    return httptransport.NewServer(
        endpoint,
        decodeDeleteRequest,
        encodeResponse,
        []httptransport.ServerOption{}...,
    )
}

func decodeDeleteRequest(ctx context.Context, r *http.Request) (interface{}, error) {
    var req pb.DeleteRequest
    if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
        return nil, err
    }
    return &req, nil
}   
func MakeSearchHandler(svc pb.MasterdataServiceServer, endpoint endpoint.Endpoint) *httptransport.Server {
    return httptransport.NewServer(
        endpoint,
        decodeSearchRequest,
        encodeResponse,
        []httptransport.ServerOption{}...,
    )
}

func decodeSearchRequest(ctx context.Context, r *http.Request) (interface{}, error) {
    var req pb.SearchRequest
    if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
        return nil, err
    }
    return &req, nil
}  

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

func RegisterHandlers(svc pb.MasterdataServiceServer, mux *http.ServeMux, endpoints endpoints.Endpoints) error {  
    log.Println("new HTTP endpoint: \"/Create\" (service=Masterdata)")
        mux.Handle("/Create", MakeCreateHandler(svc, endpoints.CreateEndpoint))    
    log.Println("new HTTP endpoint: \"/Read\" (service=Masterdata)")
        mux.Handle("/Read", MakeReadHandler(svc, endpoints.ReadEndpoint))    
    log.Println("new HTTP endpoint: \"/Update\" (service=Masterdata)")
        mux.Handle("/Update", MakeUpdateHandler(svc, endpoints.UpdateEndpoint))    
    log.Println("new HTTP endpoint: \"/Delete\" (service=Masterdata)")
        mux.Handle("/Delete", MakeDeleteHandler(svc, endpoints.DeleteEndpoint))    
    log.Println("new HTTP endpoint: \"/Search\" (service=Masterdata)")
        mux.Handle("/Search", MakeSearchHandler(svc, endpoints.SearchEndpoint))   
	return nil
}
